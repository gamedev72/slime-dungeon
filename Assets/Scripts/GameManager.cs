using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private bool isGameActive;

    [SerializeField] private int slimesInGame;
    [SerializeField] private GameObject[] slimesPrefabs;

    private GameObject titleScreen;
    [SerializeField] private GameObject player;
    
    // Start is called before the first frame update
    void Start()
    {
        titleScreen = GameObject.Find("Title Screen");
    }

    // Update is called once per frame
    void Update()
    {
        if (isGameActive)
        {
            StartCoroutine(SpawnEnemies());
        }
    }

    public void StartGame()
    {
        isGameActive = true;
        titleScreen.SetActive(false);
        player.SetActive(true);
    }

    public void StopGame()
    {
        isGameActive = false;
    }

    IEnumerator SpawnEnemies()
    {
        slimesInGame = GameObject.FindGameObjectsWithTag("Slime").Length;

        if (slimesInGame == 0)
        {
            for (int i = 0; i < 3; i++)
            {
                int n = Random.Range(0, slimesPrefabs.Length);
                Instantiate(slimesPrefabs[n], new Vector3(5 * (i * -1), 0.5f, 3), slimesPrefabs[n].transform.rotation);
            }
        }

        yield break;
    }

    public bool IsGameActive()
    {
        return isGameActive;
    }
}
