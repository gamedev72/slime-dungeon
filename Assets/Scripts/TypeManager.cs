using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TypeManager : MonoBehaviour
{
    public enum Type
    {
        Fire,
        Grass,
        Water
    }

    public static bool BeatsType(Type attacking, Type attacked)
    {
        switch (attacking, attacked)
        {
            case (Type.Fire, Type.Grass):
                return true;
            case (Type.Grass, Type.Water):
                return true;
            case (Type.Water, Type.Fire):
                return true;
        }

        return false;
    }

    public static Color GetTypeColor(Type type)
    {
        switch (type)
        {
            case Type.Fire:
                return Color.red;
            case Type.Grass:
                return Color.green;
            case Type.Water:
                return Color.blue;
        }

        return Color.clear;
    }
}
