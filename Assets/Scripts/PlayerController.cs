using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using static TypeManager;

public class PlayerController : MonoBehaviour
{
    public TypeManager.Type playerType;
    
    private float horizontalInput;
    private float verticalInput;
    private Vector3 direction;
    [SerializeField] private float speed = 1.0f;
    [SerializeField] private float rotationSpeed = 1.0f;

    private GameObject playerModel;
    private Animator playerAnim;

    [SerializeField] private GameObject hitbox;

    [SerializeField] private GameObject typeIndicator;

    [SerializeField] private GameManager gameManager;
    
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        
        playerType = TypeManager.Type.Fire;
        
        playerModel = GameObject.Find("Player Model");
        playerAnim = playerModel.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.IsGameActive())
        {
            StartCoroutine(AnimatePlayer());
            StartCoroutine(TypeOfPlayer());
            MovePlayer();
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartCoroutine(SwordAttack());
            }
        }
    }

    void MovePlayer()
    {
        verticalInput = Input.GetAxis("Vertical");
        horizontalInput = Input.GetAxis("Horizontal");

        Vector3 direction = new Vector3(horizontalInput, 0, verticalInput);
        
        transform.position += direction * speed * Time.deltaTime;
        if (horizontalInput != 0 || verticalInput != 0)
        {
            transform.forward = direction;
        }
    }

    IEnumerator AnimatePlayer()
    {
        if (horizontalInput != 0 || verticalInput != 0)
        {
            playerAnim.SetBool("Moving", true);
        }
        else
        {
            playerAnim.SetBool("Moving", false);
        }
        
        playerModel.transform.position = transform.position + new Vector3(0,-0.5f,0);
        
        yield break;
    }

    IEnumerator SwordAttack()
    {
        hitbox.SetActive(true);
        playerAnim.SetTrigger("Attack");
        yield return new WaitForSeconds(0.25f);
        hitbox.SetActive(false);
    }

    IEnumerator TypeOfPlayer()
    {
        MeshRenderer renderer = typeIndicator.GetComponent<MeshRenderer>();
        Material material;

        material = renderer.material;

        material.color = TypeManager.GetTypeColor(playerType);

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            switch (playerType)
            {
                case TypeManager.Type.Fire:
                    playerType = TypeManager.Type.Grass;
                    break;
                case TypeManager.Type.Grass:
                    playerType = TypeManager.Type.Water;
                    break;
                case TypeManager.Type.Water:
                    playerType = TypeManager.Type.Fire;
                    break;
            }
        }
        
        yield break;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Slime"))
        {
            Slime slime = collision.gameObject.GetComponent<Slime>();

            if (TypeManager.BeatsType(slime.slimeType, playerType))
            {
                PlayerDeath();
            }
        }
    }

    private void PlayerDeath()
    {
        gameManager.StopGame();
        typeIndicator.SetActive(false);
        playerAnim.SetBool("Death", true);
    }
}
