using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

public class Slime : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    
    [SerializeField] public TypeManager.Type slimeType;

    [SerializeField] private GameObject player;
    [SerializeField] private PlayerController playerControllerScript;
    
    [SerializeField] private float speed = 5.0f;
    
    void Awake()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();
        player = GameObject.Find("Player");
        playerControllerScript = player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (gameManager.IsGameActive())
        {
            MoveSlime();
        }
    }

    void MoveSlime()
    {
        Vector3 direction = (player.transform.position - transform.position).normalized;
        transform.forward = direction;
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Hitbox"))
        {
            if (TypeManager.BeatsType(playerControllerScript.playerType, slimeType))
            {
                Destroy(gameObject);
            }
        }
    }
}
